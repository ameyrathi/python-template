import logging
import socket

from flask.helpers import stream_with_context
# import socketio
from codeitsuisse import app
logger = logging.getLogger(__name__)
from flask_socketio import SocketIO
import requests
from requests import Session, Request
from flask import request
import json 
import sseclient 
import urllib
import urllib3

@app.route('/', methods=['GET'])
def default_route():
    return "Python Template"


logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

socketio = SocketIO(app)
client = ''
player, opponent = '', ''
logger = logging.getLogger(__name__)

board = [
    [ '_', '_', '_' ],
    [ '_', '_', '_' ],
    [ '_', '_', '_' ]
]
indexes = {'NW': [0, 0], 'N': [0,1], 'NE': [0,2],
            'W': [1, 0], 'C': [1, 1], 'E': [1, 2],
            'SW': [2, 0], 'S': [2, 1], 'SE': [2, 2]
            }
inverse_index = {}
for key, val in indexes.items():
    inverse_index[str(val)] = key

# def handle_message(message):
#     global player, opponent
    
#     print('received json: ' + str(message))
#     data = json.loads(message)   

#     if 'youAre' in data.keys():
#         player = data['youAre']
#         if player == 'x':
#             opponent = 'o'  
#         else:
#             opponent = 'x'
    
#     if 'action' in data.keys():
#         isValid = update_board(data)
#         if not isValid:
#             response = {"action": "(╯°□°)╯︵ ┻━┻"}
        
#         if data['player'] == player:
#             return None
#         else:
#             update_board(data)
#             bestMove = findBestMove(board)
#             move = inverse_index[str(bestMove)]
#             response = {"action": "putSymbol", "position": move}
#         requests.post(f'https://cis2021-arena.herokuapp.com/tic-tac-toe/play/{battleId}', response)
#     return 'success'

def get_stream(url):
    s = requests.Session()
    global player, opponent
    global board 

    with s.get(url, headers=None, stream=True) as resp:
        for line in resp.iter_lines():
            
            if line:
                data = eval(line.decode('utf-8').lstrip('data: ').rstrip('\''))
                # data = json.loads(request.data).get('data')
                # if data == None:
                #     data = json.loads(line.decode('utf-8'))
                print(data)
                if data != None:
                    print(type(data), data)
                    if 'youAre' in data.keys():
                        player = data['youAre'].lower()
                        if player == 'x':
                            opponent = 'o'  
                        else:
                            opponent = 'x'
                            bestMove = findBestMove(board)
                            move = inverse_index[str([bestMove[0], bestMove[1]])]
                            response = {"action": "putSymbol", "position": move}
                            requests.post(url, response)
                    
                    if 'action' in data.keys():
                        isValid, board = update_board(data)
                        if not isValid:
                            response = {"action": "(╯°□°)╯︵ ┻━┻"}
                        
                        if data['player'].lower() == player:
                            return None
                        else:
                            # _, board = update_board(data)
                            bestMove = findBestMove(board)
                            move = inverse_index[str([bestMove[0], bestMove[1]])]
                            response = {"action": "putSymbol", "position": move}
                        print(response)
                        requests.post(url, response)
                else:
                    print('Skipping', line)

                
@app.route('/tic-tac-toe', methods=['POST'])
def play():
    board = [
    [ '_', '_', '_' ],
    [ '_', '_', '_' ],
    [ '_', '_', '_' ]
    ]   

    print('*'*20)
    print("received request")
    print(request.data)
    print('*'*20)
    # start game 
    global battleId
    battleId = json.loads(request.data).get('battleId')
    print(f'BattleId is > {battleId}')
    
    url = f'https://cis2021-arena.herokuapp.com/tic-tac-toe/start/{battleId}'
    get_stream(url)

    # with requests.get(url, stream=True) as r:
    #     print(request.data)
    #     print(r)

    # http = urllib3.PoolManager()
    # stream_response = http.request('GET', url, preload_content=False)
    # client = sseclient.SSEClient(stream_response)

    # # Loop forever (while connection "open")
    # for event in client.events():
    #     print('*****')
    #     print ("got a new event from server")
    #     print(json.loads(event.data))
    #     print('*****')
    
    # res = requests.get(f'https://cis2021-arena.herokuapp.com/tic-tac-toe/start/{battleId}')
    # print('*'*20)
    # print(res)
    # print('*'*20)
    # base_url = f'https://cis2021-arena.herokuapp.com/tic-tac-toe/start'
    # url = base_url + '/' + urllib.urlencode(battleId)
    # print(url)
    # http = urllib3.PoolManager()
    # stream_response = http.request('GET', url, preload_content=False)
    # client = sseclient.SSEClient(stream_response)

    # # Loop forever (while connection "open")
    # for event in client.events():
    #     print('*****')
    #     print ("got a new event from server")
    #     print(json.loads(event.data))
    #     print('*****')


        

    with requests.get(url, stream=True) as r:
        print(request.data)
    
    return 'success'
    


    
# @socketio.on('message')
# def handle_message(message):
#     global player, opponent
    
#     print('received json: ' + str(message))
#     data = json.loads(message)   

#     if 'youAre' in data.keys():
#         player = data['youAre']
#         if player == 'x':
#             opponent = 'o'  
#         else:
#             opponent = 'x'
    
#     if 'action' in data.keys():
#         isValid = update_board(data)
#         if not isValid:
#             response = {"action": "(╯°□°)╯︵ ┻━┻"}
        
#         if data['player'] == player:
#             return None
#         else:
#             update_board(data)
#             bestMove = findBestMove(board)
#             move = inverse_index[str(bestMove)]
#             response = {"action": "putSymbol", "position": move}
#         requests.post(f'https://cis2021-arena.herokuapp.com/tic-tac-toe/play/{battleId}', response)
#     return 'success'



    
    

def update_board(data):
    # {"player":"O","action":"putSymbol","position":"NW"}
    global board
    print(data)
    print(board)
    player, position = data['player'], data['position']
    index1, index2 = indexes[position]
    if board[index1][index2] != '_':
        return False, board
    board[index1][index2] = player.lower()
    print(board)
    return True, board


# Python3 program to find the next optimal move for a player

 
# This function returns true if there are moves
# remaining on the board. It returns false if
# there are no moves left to play.
def isMovesLeft(board) :
 
    for i in range(3) :
        for j in range(3) :
            if (board[i][j] == '_') :
                return True
    return False
 
# This is the evaluation function as discussed
# in the previous article ( http://goo.gl/sJgv68 )
def evaluate(b) :
   
    # Checking for Rows for X or O victory.
    for row in range(3) :    
        if (b[row][0] == b[row][1] and b[row][1] == b[row][2]) :       
            if (b[row][0] == player) :
                return 10
            elif (b[row][0] == opponent) :
                return -10
 
    # Checking for Columns for X or O victory.
    for col in range(3) :
      
        if (b[0][col] == b[1][col] and b[1][col] == b[2][col]) :
         
            if (b[0][col] == player) :
                return 10
            elif (b[0][col] == opponent) :
                return -10
 
    # Checking for Diagonals for X or O victory.
    if (b[0][0] == b[1][1] and b[1][1] == b[2][2]) :
     
        if (b[0][0] == player) :
            return 10
        elif (b[0][0] == opponent) :
            return -10
 
    if (b[0][2] == b[1][1] and b[1][1] == b[2][0]) :
     
        if (b[0][2] == player) :
            return 10
        elif (b[0][2] == opponent) :
            return -10
 
    # Else if none of them have won then return 0
    return 0
 
# This is the minimax function. It considers all
# the possible ways the game can go and returns
# the value of the board
def minimax(board, depth, isMax) :
    score = evaluate(board)
 
    # If Maximizer has won the game return his/her
    # evaluated score
    if (score == 10) :
        return score
 
    # If Minimizer has won the game return his/her
    # evaluated score
    if (score == -10) :
        return score
 
    # If there are no more moves and no winner then
    # it is a tie
    if (isMovesLeft(board) == False) :
        return 0
 
    # If this maximizer's move
    if (isMax) :    
        best = -1000
 
        # Traverse all cells
        for i in range(3) :        
            for j in range(3) :
              
                # Check if cell is empty
                if (board[i][j]=='_') :
                 
                    # Make the move
                    board[i][j] = player
 
                    # Call minimax recursively and choose
                    # the maximum value
                    best = max( best, minimax(board,
                                              depth + 1,
                                              not isMax) )
 
                    # Undo the move
                    board[i][j] = '_'
        return best
 
    # If this minimizer's move
    else :
        best = 1000
 
        # Traverse all cells
        for i in range(3) :        
            for j in range(3) :
              
                # Check if cell is empty
                if (board[i][j] == '_') :
                 
                    # Make the move
                    board[i][j] = opponent
 
                    # Call minimax recursively and choose
                    # the minimum value
                    best = min(best, minimax(board, depth + 1, not isMax))
 
                    # Undo the move
                    board[i][j] = '_'
        return best
 
# This will return the best possible move for the player
def findBestMove(board) :
    bestVal = -1000
    bestMove = (-1, -1)
 
    # Traverse all cells, evaluate minimax function for
    # all empty cells. And return the cell with optimal
    # value.
    for i in range(3) :    
        for j in range(3) :
         
            # Check if cell is empty
            if (board[i][j] == '_') :
             
                # Make the move
                board[i][j] = player
 
                # compute evaluation function for this
                # move.
                moveVal = minimax(board, 0, False)
 
                # Undo the move
                board[i][j] = '_'
 
                # If the value of the current move is
                # more than the best value, then update
                # best/
                if (moveVal > bestVal) :               
                    bestMove = (i, j)
                    bestVal = moveVal
 
    print("The value of the best Move is :", bestVal)
    print()
    return bestMove

if __name__ == "__main__":
    logging.info("Starting application ...")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('localhost', 0))
    port = sock.getsockname()[1]
    sock.close()
    app.run(port=port)
    # socketio.run(app)